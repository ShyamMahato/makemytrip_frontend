import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import "./App.css";

import Header from "./components/Header/Header";
import Login from "./components/Login/Login";
import Signup from "./components/Signup/Signup";
import FlightSearch from "./components/FlightSearch/FlightSearch";
import SearchResults from "./components/SearchResults/SearchResults";
import ReviewBooking from "./components/reviewBooking/ReviewBooking";
import Booking from "./components/booking/Booking";
import BookingSuccess from "./components/bookingSuccess/BookingSuccess";
import Profile from "./components/profile/Profile";
import MyTrips from "./components/myTrips/MyTrips";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <Login />
          <Signup />
          <Switch>
            <Redirect exact from="/" to="/FlightSearch" />
            <Route exact path="/FlightSearch" component={FlightSearch} />
            <Route exact path="/SearchResults" component={SearchResults} />
            <Route exact path="/ReviewBooking" component={ReviewBooking} />
            <Route exact path="/Booking" component={Booking} />
            <Route exact path="/BookingSuccess" component={BookingSuccess} />
            <Route exact path="/Profile" component={Profile} />
            <Route exact path="/MyTrips" component={MyTrips} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
